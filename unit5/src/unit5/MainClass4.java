package unit5;

public class MainClass4 {

	public static void main(String[] args) {
		Rectangle r = new Rectangle();
		System.out.println(r);
		r.moveTo(4, 4);
		System.out.println(r);
		System.out.println(r.area());

		Rectangle r2 = new Rectangle(new Point(1, 1), 8, 6);
		System.out.println(r2);
		System.out.println(r2.area());

		Rectangle r3 = new Rectangle(new Point(1, 1), new Point(4, 4));
		System.out.println(r3);
		System.out.println(r3.area());

		Segment s = new Segment(new Point(1, 1), new Point(4, 5));
		Rectangle r4 = new Rectangle(s);
		System.out.println(r4);
		System.out.println(r4.area());

		Rectangle ex40 = new Rectangle(new Point(5, 6), 10, 8);
		System.out.println(ex40);
		System.out.println("Bottom Left" + ex40.getBottomLeftpoint() + ", Bottom Right" + ex40.getBottomRightPoint()
				+ ", Top Left" + ex40.getTopLeftPoint() + "Top Right" + ex40.getTopRightPoint());

	}

}
