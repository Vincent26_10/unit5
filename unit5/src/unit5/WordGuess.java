package unit5;
import java.util.Scanner;

public class WordGuess {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean gameOver=false;
		String valor;
		MagicWord m=new MagicWord();
		
		while(!gameOver) {
			System.out.println("Enter a letter");
			valor = input.next();
			m.checkLetter(valor);
			System.out.println(m.printLetters());			
			gameOver=m.checkGame();
			System.out.println(checkWin(m));
		}
	}
	
	
	
	public static String checkWin(MagicWord m) {
		if(m.checkGame()) {
			return ""+m.getNumAttempts()+"\n CONGRATULATIONS, YOU WIN";
		}else {
			return ""+m.getNumAttempts();
		}
	}

}
