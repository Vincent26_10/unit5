package unit5;

public class Triangle {

	private Point v1;
	private Point v2;
	private Point v3;
	
	
	
	public Triangle(int x1,int y1,int x2,int y2,int x3,int y3) {
		v1 = new Point(x1,y1);
		v2 = new Point(x2,y2);
		v3 = new Point(x3,y3);
	}

	public Triangle(Point p1,Point p2,Point p3 ) {
		v1=p1;
		v2=p2;
		v3=p3;
	}
	
	@Override
	public String toString() {
		return "Triangle @ " + v1.toString() + "" + v2.toString() + ""+ v3.toString();
	}
	
	public double getPerimeter() {
		double per=0.0;
		per += Point.distance(v1,v2);
		per += Point.distance(v2,v3);
		per += Point.distance(v3,v1);
		return per;
	}
	
	public void printType() {
		double side1,side2,side3;
		side1 =Point.distance(v1,v2);
		side2 =Point.distance(v2,v3);
		side3 =Point.distance(v3,v1);
		
		if(side1 == side2 && side1 == side3) {
			System.out.println(" Equilateral");
		}else if(side1 == side2 || side2 == side3 || side1 == side3) {
			System.out.println("Isosceles");
		}else {
			System.out.println("Scalene");
		}
		
	}
	
}
