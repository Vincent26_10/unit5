package unit5;

public class Bicycle {
	int cadence;
	int speed;
	int gear;
	
	public Bicycle() {
		cadence=0;
		speed=0;
		gear=1;
	}
	
	public Bicycle(int cadence,int speed, int gear) {
		this.cadence=cadence;
		this.speed=speed;
		this.gear=gear;
	}
	void changeCadence(int newValue) {
		cadence = newValue;    }
	
	void changeGear(int newValue) {
		gear = newValue;    }
	
	void changeSpeedUp(int increment) {
		speed = speed + increment;       }
	
	void applyBrakes(int decrement) {
		speed = speed - decrement;    }
	
	void printStates() {
		System.out.println("cadence:" +
				cadence + " speed:" +
				speed + " gear:" + gear);    
	}

}
