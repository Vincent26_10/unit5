package unit5;

import java.util.*;

public class Ticket {
	Drum d;

	private Block[] ticket;

	public Ticket(int numBlocks) {
		ticket = new Block[numBlocks];
		for (int i = 0; i < numBlocks; i++) {
			Block b = new Block(6);
			ticket[i] = b;
		}

	}

	public Ticket(boolean full) {
		if (full) {
			ticket = new Block[8];
			d = new Drum();
			for (int i = 0; i < ticket.length; i++) {
				int[] numbers = new int[6];
				for (int j = 0; j < numbers.length; j++) {
					numbers[j] = d.extractBall();
				}
				ticket[i] = new Block(numbers);
			}

		}
	}
	
	public int watchRemainingBall() {
		return d.watchBall();
	}

	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < ticket.length; i++) {
			s += ticket[i].toString() + "\n";
		}
		return s;
	}

	public void printUsedNumbers() {
		int[] nums = new int[50];

		for (int i = 0; i < nums.length; i++) {
			nums[i] = 0;
		}

		for (int i = 0; i < ticket.length; i++) {
			for (int j = 0; j < ticket[0].getNumbers().length; j++) {
				nums[ticket[i].getNumbers()[j]] = ticket[i].getNumbers()[j];
			}
		}
		Arrays.sort(nums);
		boolean v = true;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] != 0) {
				if (v) {
					System.out.print(nums[i]);
					v = false;
				} else {
					System.out.print("," + nums[i]);
				}
			}
		}
		System.out.println();

	}

}