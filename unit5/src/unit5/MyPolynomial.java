package unit5;

import java.io.*;
import java.util.*;

public class MyPolynomial {
	private double[] coeffs;

	public MyPolynomial(double... coeffs) {
		this.coeffs = coeffs;

	}

	public MyPolynomial(int degree) {
		for (int i = 0; i <= degree; i++) {
			coeffs[i] = 0.0;
		}
	}

	public MyPolynomial(String filename) {
		Scanner in = null;
		try {
			in = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int degree = in.nextInt();
		coeffs = new double[degree + 1];
		for (int i = 0; i < coeffs.length; ++i) {
			coeffs[i] = in.nextDouble();
		}
	}

	public int getDegree() {
		return coeffs.length - 1;
	}

	@Override
	public String toString() {

		String s = "";
		String term;
		for (int i = 0; i < coeffs.length; i++) {
			if (coeffs[i] != 0) {
				if (i == 0) {
					term = "" + coeffs[i];
				} else {
					if (i == 1) {
						term = coeffs[i] + "x";
					} else {
						term = coeffs[i] + "x^" + i;
					}
				}
				if (s.length() != 0) {
					s = term + "+" + s;
				} else {
					s = term + s;
				}
			}

		}

		return s;

	}

	public double evaluate(double x) {
		double result = 0;
		for (int i = 0; i < coeffs.length; i++) {
			result += coeffs[i] * Math.pow(x, i);
		}
		return result;

	}

	public MyPolynomial add(MyPolynomial coeffs2) {
		if (coeffs.length >= coeffs2.coeffs.length) {
			for (int i = 0; i < coeffs.length; i++) {
				coeffs2.coeffs[i] += coeffs[i];
			}
			return coeffs2;
		} else {
			for (int i = 0; i < coeffs2.coeffs.length; i++) {
				coeffs2.coeffs[i] += coeffs[i];
			}
			return coeffs2;
		}
	}

	public MyPolynomial addProfe(MyPolynomial p) {
		double[] result, biggest, smallest;
		if (coeffs.length - 1 >= p.coeffs.length) {
			biggest = coeffs;
			smallest = p.coeffs;
		} else {
			biggest = p.coeffs;
			smallest = coeffs;
		}
		result = new double[biggest.length];
		for (int i = 0; i < biggest.length; i++) {
			result[i] = biggest[i];
		}

		for (int i = 0; i < smallest.length; i++) {
			result[i] += smallest[i];
		}
		return new MyPolynomial(result);
	}

	public MyPolynomial mutiply(MyPolynomial coeffs2) {
		double[] result = new double[coeffs.length + coeffs2.coeffs.length - 1];
		double val = 0;
		int degree = 0;
		
		for(int i=0;i<result.length;i++) {
			result[i]=0;
		}

		for (int i = 0; i < coeffs.length; i++) {
			for (int j = 0; j < coeffs2.coeffs.length; j++) {
				val = coeffs[i] * coeffs2.coeffs[j];
				degree = i + j;

				result[degree] += val;

			}
		}

		return new MyPolynomial(result);
	}
}
