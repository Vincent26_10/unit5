package unit5;


public class Rectangle extends Point {

	private int width;
	private int height;

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Rectangle() {
		super();
		width=0;
		height=0;
	}
	
	public Rectangle(Point p, int width,int height) {
		super(p.getX(),p.getY());
		this.width = width;
		this.height= height;
		
	}
	
	public Rectangle(Point p1,Point p2) {
		super(p1.getX(),p1.getY());
		height= p2.getX() - p1.getX();
		width= p2.getY() - p1.getY();
	}
	
	public Rectangle (Segment s) {
		super(s.getStartPoint().getX(),s.getStartPoint().getY());
		Point p1 = s.getStartPoint();
		Point p2 = s.getEndPoint();
		width = p2.getX() -  p1.getX();
		height = p2.getY() - p1.getY();		
	}
	
	public double area() {
		return width * height;
	}
	
	public Point getBottomLeftpoint() {
		return new Point(getX(),getY());
	}
	
	public Point getTopRightPoint() {
		return new Point(getX() + width, getY() + height);
	}
	
	public Point getTopLeftPoint() {
		return new Point(getX() , getY() + height);
	}
	
	public Point getBottomRightPoint() {
		return new Point(getX() + width, getY());
	}
	
	@Override
	public String toString(){
		return super.toString()+ "  width:" + width + "   height:" + height;
	}
}
