package unit5;

public class MyPolynomialMain {

	public static void main(String[] args) {
		MyPolynomial p1 = new MyPolynomial(1.1, 2.2, 3.3);
		MyPolynomial p2 = new MyPolynomial(1.1, 2.2, 3.3, 4.4, 5.5);
		double[] coeffs = { 1.2, 3.4, 5.6, 7.8 };
		MyPolynomial p3 = new MyPolynomial(coeffs);
		MyPolynomial p4 = new MyPolynomial("poly");
		MyPolynomial p5 = new MyPolynomial(1, 1, 1);
		MyPolynomial p6 = new MyPolynomial(1, 1, 1);
		MyPolynomial p7 = p6.mutiply(p5);
		System.out.println(p7.toString());
	}

}
