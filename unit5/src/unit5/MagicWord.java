package unit5;

public class MagicWord {

	private static final String[] MATRIX = { "yellow", "mom", "words", "car", "phone", "computer", "laptop"};
	private boolean[] letter;
	private String word;
	private int selectWord;
	private int letterLength;
	private int attempts;
	private boolean endGame;

	public MagicWord() {
		selectWord = (int) (Math.random() * MATRIX.length);
		word = MATRIX[selectWord];
		letterLength = 0;
		attempts = 0;
		endGame = false;
		initLetter();
	}

	public void initLetter() {
		letter = new boolean[word.length()];
		for (int i = 0; i < letter.length; i++) {
			letter[i] = false;
		}

	}

	public void checkLetter(String letter) {

		letterLength = letter.length();
		if (letterLength == 1) {
			for (int i = 0; i < this.letter.length; i++) {
				if (letter.equals(word.substring(i, i + 1))) {
					this.letter[i] = true;
				}
			}
			attempts++;
		} else if (letterLength > 1) {
			if (letter.equals(word)) {
				endGame = true;
			}
			attempts++;
		} else {
			System.err.println("No has pasado nada");
			attempts++;
		}

	}

	public String printLetters() {
		String s = "";
		for (int i = 0; i < letter.length; i++) {
			if (letter[i]) {
				s += (word.substring(i, i + 1));
			} else {
				s += ("_");
			}
		}
		return /*"Attempts"+attempts+"\n "+*/s;

	}

	public boolean checkGame() {
		int cont = 0;
		for (int i = 0; i < letter.length; i++) {
			if (letter[i]) {
				cont++;
			}
		}
		if (cont == word.length()) {
			endGame = true;
			return endGame;
		} else {

			return endGame;
		}
	}
	
	public int getNumAttempts() {
		return attempts;
	}
	
	

}
