package unit5;

public class Drum {
	private int[] numbers;
	public static final int MAX_NUMBER=49;
	int numberMax=MAX_NUMBER-1;
	
	public Drum() {
		numbers = new int[MAX_NUMBER];
		for(int i =1;i<=MAX_NUMBER;i++){
			numbers[i-1]=i;
		}
		shuffleDrum();
	}
	
	
	public int extractBall() {
		int m=numbers[numberMax];
		numberMax--;
		return m;
		
		
	}


	public void shuffleDrum() {
		int n =0;
		for (int i = 0; i < 1000; i++) {
			int x = (int) (Math.random() * 49);
			int y = (int) (Math.random() * 49);
			n = numbers[y];
			numbers[y] = numbers[x];
			numbers[x] = n;
		}
	}
	
	public int watchBall() {
		return numbers[numberMax--];
	}

}
